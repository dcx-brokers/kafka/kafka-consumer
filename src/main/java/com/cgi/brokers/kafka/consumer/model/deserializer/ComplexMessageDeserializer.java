package com.cgi.brokers.kafka.consumer.model.deserializer;

import java.util.Map;

import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cgi.brokers.kafka.consumer.model.ComplexMessage;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Deserializer pour un {@link ComplexMessage}.
 *
 * @author damien.cacheux
 */
public class ComplexMessageDeserializer implements Deserializer<ComplexMessage>
{
    /**
     * Logger de la classe.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ComplexMessageDeserializer.class);

    @Override
    public void configure(final Map<String, ?> theMap, final boolean theB)
    {

    }

    @Override
    public ComplexMessage deserialize(final String theS, final byte[] theBytes)
    {
        final ObjectMapper mapper = new ObjectMapper();
        ComplexMessage complexMessage = null;
        try
        {
            complexMessage = mapper.readValue(theBytes, ComplexMessage.class);
        } catch (final Exception e)
        {

            e.printStackTrace();
        }
        return complexMessage;
    }

    @Override
    public void close()
    {

    }
}
